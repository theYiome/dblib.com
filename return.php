<?php
    session_start();
    if(isset($_SESSION["id"]) && isset($_POST["id_wypozyczenia"])) {

        $pesel = $_SESSION["id"];
        
        require_once("db.php");
        $pdo = db_connect();
        $sql = "UPDATE projekt.wypozyczenia SET data_oddania=CURRENT_DATE WHERE pesel_czytelnika=? AND id_wypozyczenia=?";
        $stmt = $pdo->prepare($sql);

        try {
            $stmt->execute([$pesel, $_POST["id_wypozyczenia"]]);
            header("Location: index.php");
        } catch (Exception $e) {
            header("Location: insert/badInsert.php?error=" . urlencode($e->getMessage()));
            die();
        }
        
    } else {
        header("Location: insert/badPost.php");
    }
    
?>