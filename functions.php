<?php
    function echo_login_form() {
        echo '
            <form action="login.php" method="post" class="login-form shadow wrap">
                <div class="vertical black shadow max">Czytelnku, zaloguj się!</div>
                <input type="number" name="pesel" placeholder="PESEL" class="vertical black shadow">
                <input type="text" name="password" placeholder="Hasło" class="vertical black shadow">
                <input type="submit" value="Zaloguj" class="vertical black shadow">
            </form>
        ';
    }

    function echo_register_form() {
        echo '
            <form action="register.php" method="post" class="login-form shadow wrap">
                <div class="vertical black shadow max">Jeżeli nie masz jeszcze konta, zarejestruj się!</div>
                <input type="number" name="pesel" placeholder="PESEL" class="vertical black shadow">
                <input type="text" name="password" placeholder="Hasło" class="vertical black shadow">
                <input type="text" name="fname" placeholder="Imię" class="vertical black shadow">
                <input type="text" name="lname" placeholder="Nazwisko" class="vertical black shadow">
                <input type="submit" value="Zerejestruj" class="vertical black shadow">
            </form>
        ';
    }

    function echo_logout_form() {
        echo '
            <form action="logout.php" method="post" class="login-form shadow entry wrap">
                <div class="vertical shadow book" style="background-color: white; color: black; padding: 16px;">
                    Jeżeli chcesz się wylogować kliknij przycisk "Wyloguj"
                </div> 
                <input type="submit" value="Wyloguj" class="vertical get shadow">
            </form>
        ';
    }


    function hidden_input1($id) {
        return '
            <form action="borrow.php" method="post" class="vertical get shadow">
                <input type="hidden" name="id_kopii" value="' . $id . '">
                <input type="submit" value="Wypożycz">
            </form>
        ';
    }

    function hidden_input2($id) {
        return '
            <form action="return.php" method="post" class="vertical get shadow">
                <input type="hidden" name="id_wypozyczenia" value="' . $id . '">
                <input type="submit" value="Oddaj">
            </form>
        ';
    }

    function echo_admin_panel() {
        echo '
            <a href="adminPanel.php"><div class="block shadow">Dodaj nowy asortyment</div></a>
        ';
    }

    function echo_full_table($tableName) {
        require_once("db.php");
        $pdo = db_connect();

        $sql = "SELECT * FROM " . $tableName;
        $stmt = $pdo->query($sql);
        $data = $stmt->fetchAll();

        $tempColumns = array_keys($data[0]);
        $columns = array();
        foreach($tempColumns as $key) {
            if(!is_numeric($key)) array_push($columns, $key);
        }

        echo '<div class="block shadow title_block black">Raport tabeli <b>' . $tableName . '</b></div>';
        echo '<div class="block shadow">';
        echo '<table>';
        echo '<tr>';
        foreach($columns as $key) {
            echo '<th>' . $key . '</th>';
        }
        echo '</tr>';
        
        foreach($data as $row) {
            echo '<tr>';
            foreach($columns as $key) {
                echo '<td>' . $row[$key] . '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';
        echo '</div>';
    }

    function echo_available_form($buttons) {
        require_once("db.php");
                
        $pdo = db_connect();
        
        $stmt = $pdo->query("SELECT * FROM projekt.dostepne x LEFT JOIN projekt.dziela_kategorie y ON x.nazwa_dziela=y.nazwa_dziela ORDER BY x.nazwa_dziela");
        $available = $stmt->fetchAll();
        // array_push($available, $row);
        
        $stmt = $pdo->query("SELECT id_jednostki, miasto, adres, nazwa FROM projekt.jednostki ORDER BY nazwa");
        echo '<div class="block shadow brick1 title_block">Dzieła <b>dostępne</b> do wyporzyczenia w poszczególnych odziałach <b>posortowane alfabetycznie wedle nazwy dzieła</b></div>';
        while ($row = $stmt->fetch()) {
            echo '<div class="block shadow">';
            echo '<div class="name shadow">' . $row["nazwa"] . "<br><b>" . $row["miasto"] . "</b> " . $row["adres"] . '</div>';
            foreach ($available as $entry) {
                if($entry["id_jednostki"] == $row["id_jednostki"]) {
                    echo '<div class="entry shadow">';
                    echo '<div class="vertical max shadow wrap">';
                    echo '<div class="brick brick1 shadow">' . $entry["nazwa_dziela"] . '</div>';
                    echo '<div class="brick brick2 shadow">' . $entry["nazwa_jezyka"] . '</div>';
                    echo '<div class="brick brick3 shadow">' . $entry["stan_fizyczny"] . '</div>';
                    echo '<div class="brick brick2 shadow">' . $entry["nazwa_kategorii"] . '</div>';
                    echo '</div>';
                    if($buttons) echo hidden_input1($entry["id_kopii"]);
                    echo '</div>';
                }
            }
            echo '</div>';
        }
    }

    
    function echo_wypozyczone_form($pesel) {
        require_once("db.php");

        $pdo = db_connect();
        
        $sql = "SELECT *, (CURRENT_DATE - x.termin_oddania) AS diff FROM projekt.wypozyczenia x LEFT JOIN projekt.asortyment y ON x.id_kopii=y.id_kopii WHERE pesel_czytelnika=? AND data_oddania IS NULL";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$pesel]);
        
        $row = $stmt->fetchAll();
        
        if(count($row) > 0) {
            echo '
            <div class="block shadow brick1 title_block">
            Dzieła wypożyczone
            </div>
            ';
    
            foreach ($row as $entry) {
                echo '<div class="entry shadow">';
                echo '<div class="vertical max shadow wrap">';
                echo '<div class="brick brick3 shadow">' . $entry["nazwa_dziela"] . '</div>';
                echo '<div class="brick brick1 shadow">Data wypożyczenia: ' . $entry["data_wypozyczenia"] . '</div>';
                echo '<div class="brick brick2 shadow">Termin oddania: ' . $entry["termin_oddania"] . '</div>';
                echo '<div class="brick brick1 shadow">' . $entry["nazwa_jezyka"] . '</div>';
                echo '<div class="brick brick3 shadow">' . $entry["stan_fizyczny"] . '</div>';
                if($entry["diff"] > 0) echo '<div class="brick black shadow">' . $entry["diff"] . ' dni po terminie oddania!</div>';
                echo '</div>';
                echo hidden_input2($entry["id_wypozyczenia"]);
                echo '</div>';
            }
        }
    }
    
    function get_user_data($id) {
        require_once("db.php");
        $pdo = db_connect();
        $sql = 'SELECT * FROM projekt.czytelnicy WHERE pesel_czytelnika=? LIMIT 1';
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$id]);
        return $stmt->fetch();
    }

    function echo_number_of_users() {
        require_once("db.php");
        $pdo = db_connect();
        $sql = 'SELECT COUNT(*) AS nr FROM projekt.czytelnicy';
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        echo '<div class="block shadow brick1 title_block">Mamy już <b>';
        echo $stmt->fetch()["nr"];
        echo '</b> zarejestrowanych użytkowników, dołącz do nas!</div>';
    }

    function echo_dziela() {
        require_once("db.php");
                
        $pdo = db_connect();
        
        $stmt = $pdo->query("SELECT * FROM projekt.dziela ORDER BY nazwa_dziela");
        $data = $stmt->fetchAll();
        
        echo '<div class="block shadow title_block black">Spis dzieł</div>';
        echo '<div class="block shadow">';
        echo '<table>';

        echo '<tr>';
        echo '<th>Nazwa dzieła</th><th>Nazwa autora</th><th>Data powstania</th><th>Opis</th>';
        echo '</tr>';
        
        foreach($data as $row) {
            echo '<tr>';
            echo '<td>' . $row['nazwa_dziela'] . '</td>';
            echo '<td>' . $row['nazwa_autora'] . '</td>';
            echo '<td>' . $row['data_powstania'] . '</td>';
            echo '<td>' . $row['opis'] . '</td>';
            echo '</tr>';
        }
        echo '</table>';
        echo '</div>';
    }

    function echo_autorzy() {
        require_once("db.php");
                
        $pdo = db_connect();
        
        $stmt = $pdo->query("SELECT * FROM projekt.autorzy ORDER BY nazwa_autora");
        $data = $stmt->fetchAll();
        
        echo '<div class="block shadow title_block black">Spis autorów</div>';
        echo '<div class="block shadow">';
        echo '<table>';

        echo '<tr>';
        echo '<th>Nazwa autora</th><th>Data urodzenia</th><th>Opis</th>';
        echo '</tr>';
        
        foreach($data as $row) {
            echo '<tr>';
            echo '<td>' . $row['nazwa_autora'] . '</td>';
            echo '<td>' . $row['data_urodzenia'] . '</td>';
            echo '<td>' . $row['opis'] . '</td>';
            echo '</tr>';
        }
        echo '</table>';
        echo '</div>';
    }
?>