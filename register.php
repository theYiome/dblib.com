<?php
    session_start();
    if(isset($_POST["pesel"]) && isset($_POST["password"]) && isset($_POST["fname"]) && isset($_POST["lname"])) {

        $pesel = $_POST["pesel"];
        $password = $_POST["password"];
        $lname = $_POST["lname"];
        $fname = $_POST["fname"];

        require_once("db.php");
        $pdo = db_connect();

        $sql = "INSERT INTO projekt.czytelnicy (pesel_czytelnika, haslo, imie, nazwisko, data_rejestracji) VALUES (?, ?, ?, ?, CURRENT_DATE)";
        $stmt = $pdo->prepare($sql);

        try { 
            $result = $stmt->execute([$pesel, $password, $lname, $fname]);
        } catch (Exception $e) {
            header("Location: insert/badInsert.php?error=" . urlencode($e->getMessage()));
            die();
        }
        
        $_SESSION["id"] = $pesel;
        header("Location: index.php");
        
    } else {
        header("Location: insert/badPost.php");
    }
    
?>