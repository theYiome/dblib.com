<!DOCTYPE html>
<html lang="pl">

    <head>
        <meta charset="utf-8"/>
        <title>Panel Administratora</title>
        <meta name="author" content="Kamil Pasterczyk"/>
        <link rel="stylesheet" href="stylesheet.css" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300&display=swap" rel="stylesheet">
    </head>

    <body>

        <div class="block center">

            <?php
                require_once("functions.php");
                require_once("echoForms.php");
                session_start();
                // sprawdz czy zalogowany
                if(!isset($_SESSION["id"])) {
                    // nie zalogowany
                        echo '
                        <a href="index.php">
                            <div class="block shadow black" style="text-align: center;">
                                Nie jesteś zalogowany, kliknij tutaj aby przejść do Strony Głównej
                            </div>
                        </a>
                    ';
                    die();
                } else {
                    $id = $_SESSION["id"];
                    $user = get_user_data($id);
                    if(!$user['administrator']) {
                        // nie administrator
                        echo '
                            <a href="index.php">
                                <div class="block shadow black" style="text-align: center;">
                                    Nie jesteś administratorem, kliknij tutaj aby przejść do Strony Głównej
                                </div>
                            </a>
                        ';
                        die();
                    } else {
                        echo '
                            <a href="index.php">
                                <div class="block shadow brick3" style="text-align: center;">
                                    Kliknij tutaj aby przejść do Strony Głównej
                                </div>
                            </a>
                        ';
                        echo_full_table('projekt.wypozyczenia');
                        echo_wypozyczenia_form();
                        echo_full_table('projekt.asortyment');
                        echo_asortyment_form();
                        echo_full_table('projekt.autorzy');
                        echo_autorzy_form();
                        echo_full_table('projekt.jednostki');
                        echo_jednostki_form();
                        echo_full_table('projekt.dziela');
                        echo_dziela_form();
                        echo_full_table('projekt.jezyki');
                        echo_jezyki_form();
                        echo_full_table('projekt.kategorie');
                        echo_kategorie_form();
                        echo_full_table('projekt.kategorie_kategorie');
                        echo_kategorie_kategorie_form();
                        echo_full_table('projekt.dziela_kategorie');
                        echo_dziela_kategorie_form();
                        echo_full_table('projekt.czytelnicy');
                        echo_czytelnicy_form();
                    }
                }

            ?>

        </div>

    </body>

</html>