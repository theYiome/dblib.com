<?php
    session_start();
    if(isset($_SESSION["id"]) && isset($_POST["id_kopii"])) {

        $pesel = $_SESSION["id"];
        
        require_once("db.php");
        $pdo = db_connect();
        $sql = "INSERT INTO projekt.wypozyczenia (id_kopii, pesel_czytelnika, data_wypozyczenia, termin_oddania) VALUES (?, ?, CURRENT_DATE, CURRENT_DATE + 30)";
        $stmt = $pdo->prepare($sql);

        try {
            $stmt->execute([$_POST["id_kopii"], $pesel]);
            header("Location: index.php");
        } catch (Exception $e) {
            header("Location: insert/badInsert.php?error=" . urlencode($e->getMessage()));
            die();
        }
        
    } else {
        header("Location: insert/badPost.php");
    }
    
?>