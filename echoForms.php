<?php
    function echo_czytelnicy_form() {
        echo '
            <form action="insert/czytelnicy.php" method="post" class="login-form shadow wrap">
                <div class="vertical black shadow max">Dodaj nowego czytelnika</div>
                <input type="number" name="pesel_czytelnika" placeholder="PESEL" class="vertical black shadow">
                <input type="text" name="haslo" placeholder="Hasło" class="vertical black shadow">
                <input type="text" name="imie" placeholder="Imię" class="vertical black shadow">
                <input type="text" name="nazwisko" placeholder="Nazwisko" class="vertical black shadow">
                <input type="number" name="administrator" placeholder="Administrator (0 lub 1)" class="vertical black shadow">
                <input type="submit" value="Dodaj" class="vertical black shadow">
            </form>
        ';
    }

    function echo_wypozyczenia_form() {
        echo '
            <form action="insert/wypozyczenia.php" method="post" class="login-form shadow wrap">
                <div class="vertical black shadow max">Dodaj nowe wypożyczenie</div>
                <input type="number" name="id_kopii" placeholder="ID kopii" class="vertical black shadow">
                <input type="number" name="pesel_czytelnika" placeholder="PESEL" class="vertical black shadow">
                <input type="text" name="data_wypozyczenia" placeholder="Data wypożyczenia" class="vertical black shadow">
                <input type="text" name="termin_oddania" placeholder="Termin oddania" class="vertical black shadow">
                <input type="text" name="data_oddania" placeholder="Data oddania" class="vertical black shadow">
                <input type="submit" value="Dodaj" class="vertical black shadow">
            </form>
        ';
    }

    function echo_asortyment_form() {
        echo '
            <form action="insert/asortyment.php" method="post" class="login-form shadow wrap">
                <div class="vertical black shadow max">Dodaj nowy asortyment</div>
                <input type="text" name="nazwa_dziela" placeholder="Nazwa dzieła" class="vertical black shadow">
                <input type="number" name="id_jednostki" placeholder="ID jednostki" class="vertical black shadow">
                <input type="text" name="nazwa_jezyka" placeholder="Nazwa języka" class="vertical black shadow">
                <input type="text" name="stan_fizyczny" placeholder="Stan fizyczny" class="vertical black shadow">
                <input type="submit" value="Dodaj" class="vertical black shadow">
            </form>
        ';
    }

    function echo_autorzy_form() {
        echo '
            <form action="insert/autor.php" method="post" class="login-form shadow wrap">
                <div class="vertical black shadow max">Dodaj nowego autora</div>
                <input type="text" name="nazwa_autora" placeholder="Nazwa autora" class="vertical black shadow">
                <input type="text" name="data_urodzenia" placeholder="Data urodzenia" class="vertical black shadow">
                <input type="text" name="opis" placeholder="Opis" class="vertical black shadow">
                <input type="submit" value="Dodaj" class="vertical black shadow">
            </form>
        ';
    }

    function echo_jednostki_form() {
        echo '
            <form action="insert/jednostki.php" method="post" class="login-form shadow wrap">
                <div class="vertical black shadow max">Dodaj nową jednostkę</div>
                <input type="text" name="miasto" placeholder="Miasto" class="vertical black shadow">
                <input type="text" name="adres" placeholder="Adres" class="vertical black shadow">
                <input type="text" name="nazwa" placeholder="Opis" class="vertical black shadow">
                <input type="submit" value="Dodaj" class="vertical black shadow">
            </form>
        ';
    }

    function echo_dziela_form() {
        echo '
            <form action="insert/dziela.php" method="post" class="login-form shadow wrap">
                <div class="vertical black shadow max">Dodaj nowe dzieło</div>
                <input type="text" name="nazwa_dziela" placeholder="Nazwa dzieła" class="vertical black shadow">
                <input type="text" name="nazwa_autora" placeholder="Nazwa autora" class="vertical black shadow">
                <input type="text" name="data_powstania" placeholder="Data powstania" class="vertical black shadow">
                <input type="text" name="opis" placeholder="Opis" class="vertical black shadow">
                <input type="submit" value="Dodaj" class="vertical black shadow">
            </form>
        ';
    }

    function echo_jezyki_form() {
        echo '
            <form action="insert/jezyki.php" method="post" class="login-form shadow wrap">
                <div class="vertical black shadow max">Dodaj nowy język</div>
                <input type="text" name="nazwa_jezyka" placeholder="Nazwa języka" class="vertical black shadow">
                <input type="text" name="nazwa_panstwa" placeholder="Nazwa państwa" class="vertical black shadow">
                <input type="text" name="opis" placeholder="Opis" class="vertical black shadow">
                <input type="submit" value="Dodaj" class="vertical black shadow">
            </form>
        ';
    }

    function echo_kategorie_form() {
        echo '
            <form action="insert/kategorie.php" method="post" class="login-form shadow wrap">
                <div class="vertical black shadow max">Dodaj nową kategorię</div>
                <input type="text" name="nazwa_kategorii" placeholder="Nazwa kategorii" class="vertical black shadow">
                <input type="text" name="opis" placeholder="Opis" class="vertical black shadow">
                <input type="submit" value="Dodaj" class="vertical black shadow">
            </form>
        ';
    }

    function echo_kategorie_kategorie_form() {
        echo '
            <form action="insert/kategorie_kategorie.php" method="post" class="login-form shadow wrap">
                <div class="vertical black shadow max">Dodaj nową zależność pomiędzy kategoriami</div>
                <input type="text" name="nazwa_kategorii" placeholder="Nazwa kategorii" class="vertical black shadow">
                <input type="text" name="przynaleznosc" placeholder="Kategoria nadrzędna" class="vertical black shadow">
                <input type="submit" value="Dodaj" class="vertical black shadow">
            </form>
        ';
    }

    function echo_dziela_kategorie_form() {
        echo '
            <form action="insert/dziela_kategorie.php" method="post" class="login-form shadow wrap">
                <div class="vertical black shadow max">Dodaj nową kategorię do dzieła</div>
                <input type="text" name="nazwa_dziela" placeholder="Nazwa dzieła" class="vertical black shadow">
                <input type="text" name="nazwa_kategorii" placeholder="Nazwa kategorii" class="vertical black shadow">
                <input type="submit" value="Dodaj" class="vertical black shadow">
            </form>
        ';
    }
?>