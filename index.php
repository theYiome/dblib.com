<!DOCTYPE html>
<html lang="pl">

    <head>
        <meta charset="utf-8"/>
        <title>Biblioteka Aleksandryjska</title>
        <meta name="author" content="Kamil Pasterczyk"/>
        <link rel="stylesheet" href="stylesheet.css" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300&display=swap" rel="stylesheet">
    </head>

    <body>

        <div class="block center">

            <?php
                require_once("functions.php");
                session_start();
                echo_number_of_users();
                // logowanie
                if(!isset($_SESSION["id"])) {
                    // nie zalogowany
                    echo_login_form();
                    echo_register_form();
                    echo_available_form(FALSE);
                } else {
                    // zalogowany
                    
                    $row = get_user_data($_SESSION["id"]);

                    echo '
                        <div class="block shadow title_block black">
                            Zalogowany jako <b>' . $row["imie"] . " " . $row["nazwisko"] . '</b>
                        </div>
                    ';

                    echo_logout_form();
                    
                    if($row["administrator"]) {
                        echo '
                            <a href="adminPanel.php">
                                <div class="block shadow brick3" style="text-align: center;">
                                    Kliknij tutaj aby przejść do Panel Administratora
                                </div>
                            </a>
                        ';
                    } else {
                        echo '
                            <div class="block shadow">
                                Infolinia +48 213 125 121
                            </div>
                        ';
                    }
                    echo_wypozyczone_form($_SESSION["id"]);
                    echo_available_form(TRUE);
                }
                echo_dziela();
                echo_autorzy();
            ?>

        </div>

    </body>

</html>
