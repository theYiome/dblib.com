CREATE SCHEMA projekt;
SET search_path TO projekt;

CREATE TABLE czytelnicy(
    pesel_czytelnika varchar(11),
    haslo varchar(128),
    imie varchar(64),
    nazwisko varchar(64),
    data_rejestracji date,
    administrator boolean DEFAULT false,
    PRIMARY KEY(pesel_czytelnika)
);

INSERT INTO czytelnicy VALUES('92110577545', 'pass01', 'Klara', 'Adamczyk', '2014-02-23', true);
INSERT INTO czytelnicy VALUES('00000000000', '0', 'Grażyna', 'Krupińska', '2000-01-21', true);
INSERT INTO czytelnicy VALUES('54011638248', 'pass02', 'Lechosława', 'Dudek', '2012-11-02');
INSERT INTO czytelnicy VALUES('51030497359', 'pass03', 'Jarosław', 'Tomaszewski', '2015-01-11');
INSERT INTO czytelnicy VALUES('44112365901', 'pass04', 'Felicyta', 'Jaworska', '2012-06-16');
INSERT INTO czytelnicy VALUES('46100101894', 'pass05', 'Roman', 'Kaczmarek', '2012-07-21');

CREATE TABLE jednostki(
    id_jednostki SERIAL,
    miasto varchar(64),
    adres varchar(64),
    nazwa varchar(64),
    PRIMARY KEY(id_jednostki)
);

INSERT INTO jednostki VALUES(1, 'Warszawa', 'ul. Targowa 48 03-448', 'Ptolemeusz');
INSERT INTO jednostki VALUES(2, 'Zielona Góra', 'ul. Żeglarska 7 65-548', 'Demetriusz');
INSERT INTO jednostki VALUES(3, 'Kraków', 'ul. Dębowa 7 30-308', 'Teoftast');

CREATE TABLE jezyki(
    nazwa_jezyka varchar(64),
    nazwa_panstwa varchar(64),
    opis varchar(1024),
    PRIMARY KEY(nazwa_jezyka)
);

INSERT INTO jezyki VALUES(
    'Grecki',
    'Grecja', 
    'Grecja stanowi kolebkę całej cywilizacji zachodniej, miejsce narodzin demokracji, filozofii, igrzysk olimpijskich, wielu podstawowych twierdzeń naukowych, zachodniej literatury, historiografii, politologii oraz teatru.'
);

INSERT INTO jezyki VALUES(
    'Polski',
    'Polska', 
    'Polska jest jednym z najbardziej zalesionych krajów w Europie.'
);

INSERT INTO jezyki VALUES(
    'Angielski',
    'Anglia', 
    'Anglia to kraj stanowiący obecnie część Zjednoczonego Królestwa Wielkiej Brytanii i Irlandii Północnej. W przeszłości był niezależnym królestwem.'
);

CREATE TABLE autorzy(
    nazwa_autora varchar(64),
    data_urodzenia date,
    opis varchar(1024),
    PRIMARY KEY(nazwa_autora)
);

INSERT INTO autorzy VALUES('Euklides', '365-01-01 BC', 'Euklides prawdopodobnie kształcił się w Atenach. Najprawdopodobniej był trochę młodzy od uczniów Platona, lecz również trochę starszy od Archimedesa.');
INSERT INTO autorzy VALUES('Archimedes', '287-01-01 BC', 'Archimedes był greckim filozofem przyrody oraz matematykiem urodzonym w Syrakuzach.');
INSERT INTO autorzy VALUES('Platon', '424-01-01 BC', 'Ateńczyk, twórca tradycji intelektualnej znanej jako platonizm. Sformułował podstawy idealizmu i racjonalizmu, a poprzez swą działalność literacką i pedagogiczną wprowadził takie zagadnienia jak teoria dobra, metoda dialektyczna, teoria idei, teoria sprawiedliwości czy matematyczna teoria atomów.');

CREATE TABLE dziela(
    nazwa_dziela varchar(64),
    nazwa_autora varchar(64),
    data_powstania date,
    opis varchar(1024),
    PRIMARY KEY(nazwa_dziela),
    FOREIGN KEY(nazwa_autora) REFERENCES autorzy(nazwa_autora)
);

INSERT INTO dziela VALUES('Elementy', 'Euklides', '320-01-01 BC', 'Elementy, jedno z najsłynniejszych dzieł naukowych w historii ludzkości, ukształtowały sposób myślenia o teoriach matematycznych i stały się wzorcem do naśladowania w wielu dziedzinach nauki.');
INSERT INTO dziela VALUES('O liczeniu piasku', 'Archimedes', '250-01-01 BC', 'Dzieło o wielkich liczbach i o nieskończoności które rozszerzyło system liczbowy Greków.');
INSERT INTO dziela VALUES('O ciałach pływających', 'Archimedes', '245-01-01 BC', 'Dzieło definiujące prawa hydrostatyki i aerostatyki.');
INSERT INTO dziela VALUES('Elementy mechaniki', 'Archimedes', '240-01-01 BC', 'Opisuje podstawy mechaniki teoretycznej.');
INSERT INTO dziela VALUES('Obrona Sokratesa', 'Platon', '390-01-01 BC', 'Dzieło zaliczane do jego dialogów, przedstawiające treść trzech mów, jakie Sokrates wygłosił na procesie w 399 r. p.n.e., na którym został skazany na karę śmierci.');
INSERT INTO dziela VALUES('Gorgiasz', 'Platon', '380-01-01 BC', 'Dialog w którym retoryka zostaje doceniona jako nauka zbudowana na podstawach filozofii. Zawiera on zasady etyczno-poznawcze, którymi powinni posługiwać się retorzy.');

CREATE TABLE asortyment(
    id_kopii SERIAL,
    nazwa_dziela varchar(64),
    id_jednostki SERIAL,
    nazwa_jezyka varchar(64),
    stan_fizyczny varchar(128),
    PRIMARY KEY(id_kopii),
    FOREIGN KEY(nazwa_dziela) REFERENCES dziela(nazwa_dziela),
    FOREIGN KEY(id_jednostki) REFERENCES jednostki(id_jednostki),
    FOREIGN KEY(nazwa_jezyka) REFERENCES jezyki(nazwa_jezyka)
);

INSERT INTO asortyment VALUES(1, 'Obrona Sokratesa', 2, 'Polski', 'Bez widocznych wad.');
INSERT INTO asortyment VALUES(2, 'O ciałach pływających', 3, 'Polski', 'Bez widocznych wad.');
INSERT INTO asortyment VALUES(3, 'Gorgiasz', 3, 'Angielski', 'Dziura na stronie 3.');
INSERT INTO asortyment VALUES(4, 'Obrona Sokratesa', 1, 'Grecki', 'Niebieska plama na stronie 14.');
INSERT INTO asortyment VALUES(5, 'O liczeniu piasku', 2, 'Polski', 'Pożółkłe kartki.');
INSERT INTO asortyment VALUES(6, 'Elementy', 1, 'Angielski', 'Bez widocznych wad.');
INSERT INTO asortyment VALUES(7, 'O ciałach pływających', 1, 'Angielski', 'Bez widocznych wad.');
INSERT INTO asortyment VALUES(8, 'Elementy mechaniki', 2, 'Polski', 'Pożółkłe kartki.');

CREATE TABLE wypozyczenia(
    id_wypozyczenia SERIAL,
    id_kopii SERIAL,
    pesel_czytelnika varchar(11),
    data_wypozyczenia date,
    termin_oddania date,
    data_oddania date,
    PRIMARY KEY(id_wypozyczenia),
    FOREIGN KEY(id_kopii) REFERENCES asortyment(id_kopii),
    FOREIGN KEY(pesel_czytelnika) REFERENCES czytelnicy(pesel_czytelnika)
);

INSERT INTO wypozyczenia(id_kopii, pesel_czytelnika, data_wypozyczenia, termin_oddania, data_oddania) VALUES(5, '46100101894', '2019-02-13', '2019-08-13', NULL);
INSERT INTO wypozyczenia(id_kopii, pesel_czytelnika, data_wypozyczenia, termin_oddania, data_oddania) VALUES(6, '46100101894', '2019-01-09', '2019-07-09', '2019-04-12');
INSERT INTO wypozyczenia(id_kopii, pesel_czytelnika, data_wypozyczenia, termin_oddania, data_oddania) VALUES(8, '54011638248', '2019-09-24', '2020-03-24', NULL);

CREATE TABLE kategorie(
    nazwa_kategorii varchar(64),
    opis varchar(1024),
    PRIMARY KEY(nazwa_kategorii)
);

INSERT INTO kategorie VALUES('Filozofia', 'Systematyczne i krytyczne rozważania na temat podstawowych problemów i idei, dążące do poznania ich istoty, a także do całościowego zrozumienia świata.');
INSERT INTO kategorie VALUES('Filozofia przyrody', 'Filozofia przyrody była pierwszym obszarem rozważań filozoficznych w starożytnej Grecji. Z czasem, tematyka filozoficznoprzyrodnicza stała się tylko jednym, choć bardzo istotnym, z działów filozofii. Filozofia przyrody jest również prekursorką nowożytnej nauki, która się z niej wyodrębniła i uniezależniła.');
INSERT INTO kategorie VALUES('Fizyka', 'Fizyka to nauka przyrodnicza, zajmująca się badaniem najbardziej fundamentalnych i uniwersalnych właściwości oraz przemian materii i energii, a także oddziaływań między nimi.');
INSERT INTO kategorie VALUES('Mechanika', 'W fizyce jest to dział opisujący ruch i odkształcenie ciał materialnych lub ich części na skutek ich wzajemnych oddziaływań oraz badający stan równowagi między nimi.');
INSERT INTO kategorie VALUES('Hydrodynamika', 'Dział fizyki zajmujący się analizą ruchu płynów, gdzie przez płyny rozumie się tutaj zarówno ciecze, jak i gazy.');
INSERT INTO kategorie VALUES('Matematyka', 'Nauka dostarczająca narzędzi do otrzymywania ścisłych wniosków z przyjętych założeń, zatem dotycząca prawidłowości rozumowania. Ponieważ ścisłe założenia mogą dotyczyć najróżniejszych dziedzin myśli ludzkiej, a muszą być czynione w naukach ścisłych, technice, a nawet w naukach humanistycznych.');
INSERT INTO kategorie VALUES('Geometria', 'Dziedzina matematyki badająca dla wybranych przekształceń ich niezmienniki, od najprostszych, takich jak odległość, pole powierzchni, miara kąta, przez bardziej zaawansowane, jak krzywizna, punkt stały, czy wymiar.');
INSERT INTO kategorie VALUES('Retoryka', 'Sztuka budowania artystycznej, perswazyjnej wypowiedzi ustnej lub pisemnej, nauka o niej, refleksja teoretyczna, jak również wiedza o komunikacji słownej, obrazowej i zachowawczej pomiędzy autorem wypowiedzi a jej odbiorcami.');
INSERT INTO kategorie VALUES('Etyka', 'Dział filozofii, zajmujący się badaniem moralności i tworzeniem systemów myślowych, z których można wyprowadzać zasady moralne.');

CREATE TABLE kategorie_kategorie(
    nazwa_kategorii varchar(64),
    przynaleznosc varchar(64),
    FOREIGN KEY(nazwa_kategorii) REFERENCES kategorie(nazwa_kategorii),
    FOREIGN KEY(przynaleznosc) REFERENCES kategorie(nazwa_kategorii) 
);

INSERT INTO kategorie_kategorie VALUES('Fizyka', 'Filozofia przyrody');
INSERT INTO kategorie_kategorie VALUES('Fizyka', 'Matematyka');
INSERT INTO kategorie_kategorie VALUES('Mechanika', 'Fizyka');
INSERT INTO kategorie_kategorie VALUES('Hydrodynamika', 'Mechanika');

INSERT INTO kategorie_kategorie VALUES('Geometria', 'Matematyka');

INSERT INTO kategorie_kategorie VALUES('Retoryka', 'Filozofia');
INSERT INTO kategorie_kategorie VALUES('Etyka', 'Filozofia');

CREATE TABLE dziela_kategorie(
    nazwa_dziela varchar(64),
    nazwa_kategorii varchar(64),
    FOREIGN KEY(nazwa_dziela) REFERENCES dziela(nazwa_dziela),
    FOREIGN KEY(nazwa_kategorii) REFERENCES kategorie(nazwa_kategorii)
);

INSERT INTO dziela_kategorie VALUES('O ciałach pływających', 'Hydrodynamika');
INSERT INTO dziela_kategorie VALUES('Obrona Sokratesa', 'Retoryka');
INSERT INTO dziela_kategorie VALUES('Obrona Sokratesa', 'Etyka');
INSERT INTO dziela_kategorie VALUES('Gorgiasz', 'Retoryka');
INSERT INTO dziela_kategorie VALUES('Gorgiasz', 'Etyka');
INSERT INTO dziela_kategorie VALUES('O liczeniu piasku', 'Matematyka');
INSERT INTO dziela_kategorie VALUES('Elementy', 'Geometria');
INSERT INTO dziela_kategorie VALUES('Elementy mechaniki', 'Mechanika');


-- WIDOKI

CREATE VIEW projekt.dostepne AS 
SELECT * FROM projekt.asortyment 
WHERE id_kopii NOT IN (SELECT id_kopii FROM projekt.wypozyczenia WHERE data_oddania IS NULL);

-- FUNKCJE
         
CREATE OR REPLACE FUNCTION ifavailable() RETURNS TRIGGER AS $$
    BEGIN
        IF EXISTS(SELECT * FROM projekt.wypozyczenia WHERE id_kopii=NEW.id_kopii AND data_oddania IS NULL) THEN
            RAISE EXCEPTION 'Już wypożyczone' USING HINT = 'Nie można wypozyczyc ponownie';
            RETURN NULL;
        ELSE
            RETURN NEW;
        END IF;
    END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER checker BEFORE INSERT ON wypozyczenia
FOR EACH ROW
EXECUTE PROCEDURE ifavailable();

-------------------------------------------------------------------

CREATE OR REPLACE FUNCTION okpesel() RETURNS TRIGGER AS $$
    BEGIN
        IF LENGTH(NEW.pesel_czytelnika) = 11 THEN
            RETURN NEW;
        ELSE
            RAISE EXCEPTION 'Nieprawidłowy PESEL' USING HINT = 'Numer PESEL musi mieć 11 znaków!';
            RETURN NULL;
        END IF;
    END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER checker BEFORE INSERT OR UPDATE ON czytelnicy
FOR EACH ROW
EXECUTE PROCEDURE okpesel();
