<?php
    session_start();
    if(isset($_POST["pesel"]) && isset($_POST["password"])) {

        $pesel = $_POST["pesel"];
        $password = $_POST["password"];

        require("db.php");
        $pdo = db_connect();
        $sql = "SELECT * FROM projekt.czytelnicy WHERE pesel_czytelnika=? AND haslo=?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$pesel, $password]);
        
        $row = $stmt->fetch();

        if($row == FALSE) {
            echo "Niepoprawne dane logowania, proszę spróbować ponownie!";
        } else {
            $_SESSION["id"] = $row["pesel_czytelnika"];
            header("Location: index.php");
        }
        
    } else {
        header("Location: insert/badPost.php");
    }
    
?>