<?php
    session_start();
    if(isset($_POST["nazwa_kategorii"]) && isset($_POST["opis"])) {

        require_once("../db.php");
        $pdo = db_connect();

        $sql = "INSERT INTO projekt.kategorie (nazwa_kategorii, opis) VALUES (?, ?)";
        $stmt = $pdo->prepare($sql);
        try {
            $result = $stmt->execute([$_POST["nazwa_kategorii"], $_POST["opis"]]);
        } catch (Exception $e) {
            header("Location: badInsert.php?error=" . urlencode($e->getMessage()));
            die();
        }

        header("Location: ../adminPanel.php");
        
    } else {
        header("Location: badPost.php");
    }
    
?>