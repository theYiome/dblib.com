<!DOCTYPE html>
<html lang="pl">

    <head>
        <meta charset="utf-8"/>
        <title>Niepoprawne Wstawienie</title>
        <meta name="author" content="Kamil Pasterczyk"/>
        <link rel="stylesheet" href="../stylesheet.css" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Roboto:300&display=swap" rel="stylesheet">
    </head>

    <body>

        <div class="block center">
            <div class="block shadow title_block">
                Niestety nie udało się wstawić nowego rekordu :(
            </div>
            <div class="block shadow brick1">
                Poniżej znajduje się kod błędu
            </div>
            <div class="block shadow black">
                <?php
                    echo $_GET["error"];
                ?>
            </div>
        </div>

    </body>

</html>