<?php
    session_start();
    if(isset($_POST["pesel_czytelnika"]) && isset($_POST["haslo"]) && isset($_POST["imie"]) && isset($_POST["nazwisko"]) && isset($_POST["administrator"])) {

        require_once("../db.php");
        $pdo = db_connect();

        $sql = "INSERT INTO projekt.czytelnicy (pesel_czytelnika, haslo, imie, nazwisko, data_rejestracji, administrator) VALUES (?, ?, ?, ?, CURRENT_DATE, ?)";
        $stmt = $pdo->prepare($sql);
        try {
            $result = $stmt->execute([$_POST["pesel_czytelnika"], $_POST["haslo"], $_POST["imie"], $_POST["nazwisko"], $_POST["administrator"]]);
        } catch (Exception $e) {
            header("Location: badInsert.php?error=" . urlencode($e->getMessage()));
            die();
        }

        header("Location: ../adminPanel.php");
        
    } else {
        header("Location: badPost.php");
    }
    
?>