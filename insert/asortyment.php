<?php
    session_start();
    if(isset($_POST["nazwa_dziela"]) && isset($_POST["id_jednostki"]) && isset($_POST["nazwa_jezyka"]) && isset($_POST["stan_fizyczny"])) {

        require_once("../db.php");
        $pdo = db_connect();

        $sql = "INSERT INTO projekt.asortyment (nazwa_dziela, id_jednostki, nazwa_jezyka, stan_fizyczny) VALUES (?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        try {
            $result = $stmt->execute([$_POST["nazwa_dziela"], $_POST["id_jednostki"], $_POST["nazwa_jezyka"], $_POST["stan_fizyczny"]]);
        } catch (Exception $e) {
            header("Location: badInsert.php?error=" . urlencode($e->getMessage()));
            die();
        }

        header("Location: ../adminPanel.php");
        
    } else {
        header("Location: badPost.php");
    }
    
?>