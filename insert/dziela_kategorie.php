<?php
    session_start();
    if(isset($_POST["nazwa_dziela"]) && isset($_POST["nazwa_kategorii"])) {

        require_once("../db.php");
        $pdo = db_connect();

        $sql = "INSERT INTO projekt.dziela_kategorie (nazwa_dziela, nazwa_kategorii) VALUES (?, ?)";
        $stmt = $pdo->prepare($sql);
        try {
            $result = $stmt->execute([$_POST["nazwa_dziela"], $_POST["nazwa_kategorii"]]);
        } catch (Exception $e) {
            header("Location: badInsert.php?error=" . urlencode($e->getMessage()));
            die();
        }

        header("Location: ../adminPanel.php");
        
    } else {
        header("Location: badPost.php");
    }
    
?>