<?php
    session_start();
    if(isset($_POST["nazwa_kategorii"]) && isset($_POST["przynaleznosc"])) {

        require_once("../db.php");
        $pdo = db_connect();

        $sql = "INSERT INTO projekt.kategorie_kategorie (nazwa_kategorii, przynaleznosc) VALUES (?, ?)";
        $stmt = $pdo->prepare($sql);
        try {
            $result = $stmt->execute([$_POST["nazwa_kategorii"], $_POST["przynaleznosc"]]);
        } catch (Exception $e) {
            header("Location: badInsert.php?error=" . urlencode($e->getMessage()));
            die();
        }

        header("Location: ../adminPanel.php");
        
    } else {
        header("Location: badPost.php");
    }
    
?>