<?php
    session_start();
    if(isset($_POST["miasto"]) && isset($_POST["adres"]) && isset($_POST["nazwa"])) {

        require_once("../db.php");
        $pdo = db_connect();

        $sql = "INSERT INTO projekt.jednostki (miasto, adres, nazwa) VALUES (?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        try {
            $result = $stmt->execute([$_POST["miasto"], $_POST["adres"], $_POST["nazwa"]]);
        } catch (Exception $e) {
            header("Location: badInsert.php?error=" . urlencode($e->getMessage()));
            die();
        }

        header("Location: ../adminPanel.php");
        
    } else {
        header("Location: badPost.php");
    }
    
?>