<?php
    session_start();
    if(isset($_POST["id_kopii"]) && isset($_POST["pesel_czytelnika"]) && isset($_POST["data_wypozyczenia"]) && isset($_POST["termin_oddania"]) && isset($_POST["data_oddania"])) {

        require_once("../db.php");
        $pdo = db_connect();

        $sql = "INSERT INTO projekt.wypozyczenia (id_kopii, pesel_czytelnika, data_wypozyczenia, termin_oddania, data_oddania) VALUES (?, ?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        try {
            $result = $stmt->execute([$_POST["id_kopii"], $_POST["pesel_czytelnika"], $_POST["data_wypozyczenia"], $_POST["termin_oddania"], $_POST["data_oddania"]]);
        } catch (Exception $e) {
            header("Location: badInsert.php?error=" . urlencode($e->getMessage()));
            die();
        }

        header("Location: ../adminPanel.php");
        
    } else {
        header("Location: badPost.php");
    }
    
?>