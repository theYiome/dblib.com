<?php
    session_start();
    if(isset($_POST["nazwa_dziela"]) && isset($_POST["nazwa_autora"]) && isset($_POST["data_powstania"]) && isset($_POST["opis"])) {

        require_once("../db.php");
        $pdo = db_connect();

        $sql = "INSERT INTO projekt.dziela (nazwa_dziela, nazwa_autora, data_powstania, opis) VALUES (?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        try {
            $result = $stmt->execute([$_POST["nazwa_dziela"], $_POST["nazwa_autora"], $_POST["data_powstania"], $_POST["opis"]]);
        } catch (Exception $e) {
            header("Location: badInsert.php?error=" . urlencode($e->getMessage()));
            die();
        }

        header("Location: ../adminPanel.php");
        
    } else {
        header("Location: badPost.php");
    }
    
?>